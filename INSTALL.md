ansible-role-skeleton
=====================

To use this repo as a role skeleton/template, follow these steps:

1. Simply clone this repo to your computer
1. From the location where you keep your roles run:
    ```
    ansible-galaxy init --role-skeleton=/path/to/ansible-role-skeleton ansible-new-role-name
    ```
1. Change into the dir of your new role and:
    ```
    git init
    ```
1. Now make sure the testing wrapper script is executable:
    ```
    chmod ug+x run_test.sh
    ```
1. Now feel free to delete this file :-)
