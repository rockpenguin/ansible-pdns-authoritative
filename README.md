ansible-pdns-authoritative
=========

This role manages PowerDNS Authoritative servers.

Requirements
------------

None (see Testing, below)

Role Variables
--------------

See [defaults/main.yml](defaults/main.yml) for the role vars.  Some important vars are explained below.

Name         | Default Value | Description
-------------|---------------|------------
os_supported | false         | 'flag' that indicates whether the OS is suppoorted; by default it is false
pdns_conf_api_key | empty    | Defines the API key; can be specified in ~/.secrets/ansible-pdns-authoritative.yaml

The contents of ~/.secrets/ansible-pdns-authoritative.yaml should look like:

```
---
pdns_conf_api_key: MySuperDuperS33krit!
```

Dependencies
------------

None

Testing The Role
----------------

## Requirements

 * Virtualization (Parallels, VirtualBox, etc.)
 * Vagrant

Testing uses virtualization and Vagrant to run tests.

1. Modify the tests as needed (located in the tests folder)
1. Test the role by using the script found in the root of this project, e.g.:
   ```
   ./run-test.sh debian10 up
   ```
