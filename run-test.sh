#!/usr/bin/env bash

function usage {
    echo "USAGE: $0 test_name action"
    echo "where:"
    echo "  'test_name' is the name of the subfolder containing your tests"
    echo "  'action' is one of [destroy, init login, provision, status, up]"
    exit 1
}

if [[ $# -ne 2 ]]; then usage; fi

function cd_into_test_folder {
    # check to make sure that the test_name folder exists
    if [ ! -d ${ROLE_PATH}/tests/${TEST_NAME} ]; then echo "ERROR: Test folder ${TEST_NAME} doesn't exist"; exit 1; fi

    # change into the directory specified
    cd ${ROLE_PATH}/tests/${TEST_NAME}

    # Tell Ansible where to find ye olde roles
    export ANSIBLE_ROLES_PATH=${PWD}/roles
}

function sync_role {
        if [[ -d roles/$ROLE_NAME ]]; then rm -rf roles/$ROLE_NAME; fi
        #git clone --single-branch --branch=$(git rev-parse --abbrev-ref HEAD) $ROLE_PATH roles/$ROLE_NAME
        if [[ ! -d roles ]]; then mkdir roles; fi
        rsync -a --exclude='.git' --exclude='tests' $ROLE_PATH/ roles/$ROLE_NAME
}

TEST_NAME=$1
ACTION=$2
ROLE_PATH=$(git rev-parse --show-toplevel)
ROLE_NAME=$(basename ${ROLE_PATH})

case $ACTION in

    create|init)
        ;;

    destroy|stop)
        cd_into_test_folder
        vagrant destroy --force
        ;;

    login|ssh)
        cd_into_test_folder
        vagrant ssh
        ;;

    provision)
        cd_into_test_folder
        sync_role
        vagrant provision
        ;;

    status)
        cd_into_test_folder
        vagrant status
        ;;

    up)
        cd_into_test_folder
        if [[ $(vagrant status | grep 'running') ]]; then
            printf "VM already running. Nothing to do. Perhaps you want to provision instead? \n\n"
            usage
        fi
        sync_role
        ansible-galaxy install -r requirements.yml --force
        vagrant up
        ;;

    *)
        printf "ERROR: Unknown action: $ACTION \n\n"
        usage
        ;;
esac
